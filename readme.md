
# Cognitives Template 2

This is the static front end template for the cognitives application. It will eventually become of the selectable themes for the application. It is based on the **future marketer's** site's look and feel, but is intended to be a genericised version of it.



## List of changes
The following is a list of changes made in an effort to genericise the template:


### Article
1. Added an image-caption div
2. Added an article caption div that takes in caption partials
3. Modified styling in article.scss to allow change of caption partials' author colour based on category type
4. Added a new article__category class following card__category styling
5. Removed article__quote from article.php
6. Added article__tet class to all <p> elements in article.php to enable card styles to work in the page

### Banner
1. Removed the border bottom styling from the banner-card
2. Added margin:auto to __category:after for the card-banner
3. Removed banner "&__readtime>before display:none" in banner.scss


### Card-Mixins
1. Added a new mixin "darken 2" that has a higher opacity 


### Columns
1. Removed border-top from col-third-short and applied it to card as it is a generic styling for all cards now
2. Remove border-right from card__image
3. Changed col-quarter image__height from 140px to 136px & __text height from 44px to 42px as it caused height issues


### Footer
1. Change footer__navigation display to table & navigation__list to display: table-cell


### Header
1. Removed header's  future-marketer logo to contain a generic insert logo
2. Change background-colour of header in index.scss
3. Remove post, notification count and user profile from header
4. Removed search bar from the navigation tab to replace it in header
5. Gave header an extra class .header--category to change the border colour accordingly- added a border bottom 
6. Removed top border from header__navigation

### Index
1. Removed col-quarter
2. Removed half column and two thirds column and the the col-full block


### Reset
1. Added styling for placeholder text to be white 


### Card Styles
1. Removed icon-social class from twitter caption as it is not in the design template
2. Created a new card style for col-full  


### New File(s) Added
1. card-light2
2. search-white.svg