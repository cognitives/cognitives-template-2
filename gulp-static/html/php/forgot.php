<?php require "partials/_header.php"; ?>
<div class="page_section">
    <div class="login-page">
        <div class="container">
            <div class="tab-content forget_password">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12" id="login-social">
                        <h3 class="sub-heading">Forgot Password</h3>
                        <p class="helper-text">Please enter your Fans Unite username or Email address below to get a reset password link.</p>
                        <div class="WriteAnArticleForm fullWidth">
                            <form id="forgot-form" class="form-horizontal" action="#" method="post">
                                <div class="controls-full field-forgotform-email required">
                                    <input type="text" id="forgotform-email" class="c9n-ippt required" name="ForgotForm[email]" placeholder="Fans Unite username or Email Address" data-required="1">
                                    <div class="help-block"></div>
                                </div>

                                <button type="submit" class="btn btn-success forget_btn" name="login-button">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //End Sign Up -->
    </div>
</div>

<!-- Begin Ajax based My Platform Modal Popup -->
<div class="modal fade" id="AjaxMyPlatformModal" tabindex="-1" role="dialog" aria-labelledby="AjaxMyPlatformModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="modal-close x-close" data-dismiss="modal"><span class="sr-only">Close</span></button>

			<div class="modal-body nopadd"></div>
		</div>
	</div>
</div>
<!-- //End Ajax based Profile Modal Popup -->

<?php require "partials/_footer.php"; ?>
