<?php require("partials/_header.php") ?>

<div class="blog" style="background-image:url();">
    <div class="blog__content">
        <div class="blog-left">
            <h1 class="blog__heading">Blog Headline Title</h1>
            <p class="blog__text">Blog descriptive paragraph. Lorem ipsum dolor sit amet, consectetur do adipisicing.</p>
            <a href="#" class="button">FOLLOW +</a>
            <div class="blog__social">
                <a href="#" class="blog__social-link blog__social-link--facebook">Share</a>
                <a href="#" class="blog__social-link blog__social-link--twitter">Share</a>
                <a href="#" class="blog__social-link blog__social-link--google-plus">Share</a>
                <a href="#" class="blog__social-link blog__social-link--linkedin">Share</a>
            </div>
        </div>
        <div class="blog-right">
            <?php require "partials/cards/_author-01.php" ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-two-thirds"><?php require("partials/cards/_category-01.php") ?></div>
        <div class="col-third"><?php require("partials/cards/_category-01.php") ?></div>
    </div>
    <div class="row">
        <div class="col-two-thirds"><?php require("partials/cards/_twitter-02.php") ?></div>
        <div class="col-third"><?php require("partials/cards/_facebook-02.php") ?></div>
    </div>
    <div class="row">
        <div class="col-third"><?php require("partials/cards/_twitter-02.php") ?></div>
        <div class="col-third"><?php require("partials/cards/_media-01.php") ?></div>
        <div class="col-third"><?php require("partials/cards/_facebook-02.php") ?></div>
    </div>
</div>

<?php require("partials/_follow.php") ?>
<?php require("partials/_footer.php") ?>