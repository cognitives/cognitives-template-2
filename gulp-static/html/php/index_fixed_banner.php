<?php require("partials/_header.php") ?>

<div class="page_section">
    <div class="container">
        <div class="row">
            <div class="col-full"><?php require("partials/cards/_banner-01.php") ?></div>

            <div class="col-full"><?php require("partials/cards/_banner-02.php") ?></div>

            <div class="col-full"><?php require("partials/cards/_banner-03.php") ?></div>

            <div class="col-full"><?php require("partials/cards/_banner-04.php") ?></div>

            <div class="col-full"><?php require("partials/cards/_banner-05.php") ?></div>

            <div class="col-full"><?php require("partials/cards/_banner-06.php") ?></div>
        </div>
    </div>
</div>

<?php require("partials/_footer.php") ?>