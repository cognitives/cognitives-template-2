<?php require "partials/_header.php"; ?>

<div class="userProfilePost_page page_section">
    <section class="container">
        <div class="row">
            <!-- Begin User Profile Sidebar -->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div id="userprofile-sidebar">
                    <figure class="user-profile-pic" style="background-image:url('static/images/data/thumbnail-full-article-01.png');"></figure>
                    <div class="inner-descr">
                        <h4 class="user-name text-center">AAP Digital</h4>
                        <a href="#" class="button green button-block FollowProfileUser"><i class="fa fa-star"></i>Follow</a>
                        <a href="#" class="button green button-block"><i class="fa fa-user"></i>View Profile</a>
                        <div class="social-stuffs text-center"><a href="mailto:aap@fansunite.com.au"><i class="fa fa-envelope"></i></a></div>
                    </div>
                </div>
            </div>
            <!-- //End User Profile Sidebar -->

            <!-- Begin User Posts -->
            <div class="col-md-8 col-sm-8 col-xs-12">
                <header class="sub-header">Articles</header>
                <div id="news-posts">
                    <div class="section__content">
                        <div class="row">
                            <div class="col-third"><?php require("partials/cards/_facebook-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_youtube-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_facebook-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_youtube-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_facebook-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_youtube-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_facebook-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_youtube-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_facebook-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_youtube-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_facebook-01.php") ?></div>
                            <div class="col-third"><?php require("partials/cards/_youtube-01.php") ?></div>
                        </div>
                    </div>
                </div>
                <div id="LoadMoreArticles" class="clearfix" style="clear:both;"></div>
                <div class="loader">
                    <span><img width="24" height="24" src="static/images/loading.svg"></span>
                    <span class="text">please wait<span class="one">.</span><span class="two">.</span><span class="three">.</span></span>
                </div>
            </div>
            <!-- //End User Posts -->
        </div>
    </section>
</div>


<!-- Begin Ajax based My Platform Modal Popup -->
<div class="modal fade" id="AjaxMyPlatformModal" tabindex="-1" role="dialog" aria-labelledby="AjaxMyPlatformModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="modal-close x-close" data-dismiss="modal"><span class="sr-only">Close</span></button>

			<div class="modal-body nopadd"></div>
		</div>
	</div>
</div>
<!-- //End Ajax based Profile Modal Popup -->

<?php require "partials/_footer.php"; ?>
