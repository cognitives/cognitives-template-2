<?php require("partials/_header.php") ?>
<div class="page_section">
  <div class="container">
      <div class="row">
          <div class="col-two-thirds">
              <?php require("partials/cards/_category-01.php") ?>
          </div>
          <div class="col-third-short">
              <?php require("partials/cards/_category-01.php") ?>
              <?php require("partials/cards/_twitter-03.php") ?>
          </div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_youtube-01.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_youtube-02.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_youtube-02.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_youtube-01.php") ?></div>
      </div>

      <div class="row">
          <div class="col-third"><?php require("partials/cards/_facebook-01.php") ?></div>
          <div class="col-two-thirds"><?php require("partials/cards/_facebook-03.php") ?></div>
      </div>

      <div class="row">
          <div class="col-third"><?php require("partials/cards/_facebook-03.php") ?></div>
          <div class="col-two-thirds"><?php require("partials/cards/_facebook-02.php") ?></div>
      </div>

      <div class="row">
          <div class="col-third"><?php require("partials/cards/_facebook-02.php") ?></div>
          <div class="col-two-thirds"><?php require("partials/cards/_facebook-01.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_twitter-03.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_twitter-02.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_twitter-01.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_twitter-03.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_twitter-02.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_twitter-01.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_instagram-03.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_instagram-02.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_instagram-01.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_instagram-03.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_instagram-02.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_instagram-01.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_vimeo-03.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_vimeo-02.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_vimeo-01.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_vimeo-03.php") ?></div>
      </div>

      <div class="row">
          <div class="col-two-thirds"><?php require("partials/cards/_vimeo-02.php") ?></div>
          <div class="col-third"><?php require("partials/cards/_vimeo-01.php") ?></div>
      </div>
  </div>
  
  <div class="clearfix"></div>
  <div class="call-to-action-wrapper image-covered" style="background-image: url('static/images/background-login.jpg');">
	<div class="overlay_layer"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-8 col-xs-12 col-centered text-center">
				<div class="content-area">
					<h3 class="upper">Want to gain serious edge</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					
					<form class="form-horizontal">
						<div class="form-group">
							<label class="control-label sr-only">Enter your Email</label>
							<input type="text" placeholder="Enter your Email" class="form-control" />
                        </div>
						<div class="form-group">
							<button type="submit" class="button blue lg upper" name="contact-button">Unlock access to the review</button>
                        </div>
						<div class="form-group button-set">
							<button type="submit" class="button blue upper" name="contact-button">Submit</button>
							<button type="submit" class="button blue upper" name="contact-button">Preview</button>
                        </div>
					</form>
					
				</div>
			</div>
		</div>
	</div>
  </div>
</div>

<?php require("partials/_footer.php") ?>
