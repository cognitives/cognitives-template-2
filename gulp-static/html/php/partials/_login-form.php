<div class="login-page">
    <div class="container">
        <div class="tabbable tabbable-custom">
            <ul class="nav nav-tabs nav_tabs_two">
                <li class=""><a href="#Login2" data-toggle="tab">Login</a></li>
                <li class="active"><a href="#SignUp2" data-toggle="tab">Sign Up</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade" id="Login2">
                    <div class="row">
                        <div class="col-md-16 col-sm-16 col-xs-16" id="login-social">
                            <div class="social-buttons">
                                <ul class="row">
                                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i> Login with Facebook</a></li>
                                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i> Login with Twitter</a></li>
                                    <li class="g-plus"><a href="#"><i class="fa fa-google-plus"></i> Login with Google+</a></li>
                                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i> Login with LinkedIn</a></li>
                                </ul>
                            </div>
                            <p class="text-center mbt50"><a href="#" class="txt-link">Don't have an account? <span class="green">Sign up here</span></a></p>

                            <p class="email-login"><span class="seperation">Login with email</span></p>
                            <div class="WriteAnArticleForm">
                                <form name="" class="form-horizontal" action="#" method="post">
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Warning!</strong> Better check yourself, you're not looking too good.
                                    </div>
                                    
                                    <div class="controls-full has-error"><input type="text" placeholder="Email" class="form-control" name="Email"></div>
                                    <div class="controls-full has-success"><input type="password" placeholder="Password" class="form-control" name="Password"></div>
                                    
                                    <div class="button-set">
                                        <label><input type="checkbox" id="RemeberMe" name="RemeberMe" value="RemeberMe"><span><!-- fake checkbox --></span>Remeber Me</label>
                                        <div class="help-block"></div>
                                        <a href="forgot.php" class="forget"><span class="green">Forgot Password?</span></a>
                                    </div>
                                    <button type="submit" class="button green button-large">Login</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade in active" id="SignUp2">
                    <div class="row">
                        <div class="col-md-16 col-sm-16 col-xs-16" id="signup-social">
                            
                            <p class="email-login"><span class="seperation">Let’s get this party started!</span></p>
                            
                            <div class="social-buttons">
                                <ul class="row">
                                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i> Sign up with Facebook</a></li>
                                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i> Sign up with Twitter</a></li>
                                    <li class="g-plus"><a href="#"><i class="fa fa-google-plus"></i> Sign up with Google+</a></li>
                                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i> Sign up with LinkedIn</a></li>
                                </ul>
                            </div>
                            <p class="text-center mbt50"><a href="#" class="txt-link">Already have an account? <span class="green">Login here</span></a></p>

                            <p class="email-login"><span class="seperation">Sign up with email</span></p>
                            <div class="WriteAnArticleForm">
                                <form name="" class="form-horizontal" action="#" method="post">
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Warning!</strong> Better check yourself, you're not looking too good.
                                    </div>
                                    
                                    <div class="controls-lt has-error"><input type="text" placeholder="First name" class="form-control" name="FirstName"></div>
                                    <div class="controls-rt has-success"><input type="text" placeholder="Last name" class="form-control" name="LastName"></div>
                                    <div class="controls-full"><input type="text" placeholder="Email" class="form-control" name="Email"></div>
                                    <div class="controls-full"><input type="password" placeholder="Password" class="form-control" name="Password"></div>
                                    <div class="controls-full"><input type="password" placeholder="Verify password" class="form-control" name="VerifyPassword"></div>
                                    <div class="controls-full"><input type="text" placeholder="Fans Unite username" class="form-control" name="Username"></div>
                                    <p class="helper-text">This is how you’ll be known on Fansunite</p>
                                    
                                    <div class="button-set">
                                        <label><input type="checkbox" id="AcceptTerms" name="AcceptTerms" value="AcceptTerms"><span><!-- fake checkbox --></span>I accept terms of use by clicking this checkbox</label>
                                        <div class="help-block"></div>
                                        <a href="forgot.php" class="forget"><span class="green">Forgot Password?</span></a>
                                    </div>
                                    <button type="submit" class="button green button-large">Sign Up</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //End Sign Up -->
</div>