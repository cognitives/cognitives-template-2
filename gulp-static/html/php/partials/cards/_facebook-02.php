<a href="#" class="card card__facebook card__withoutImage">
    <div class="card__no-image"></div>
    <div class="card__content">
        <div class="card__category">facebook</div>
        <p class="card__text">
            User text nibh veliod del of Lorem Ipsum. Proin gravida nibh veliod del mutte.
            <span class="card__tag">#tempor</span>
        </p>
        <?php require "partials/cards/_caption-03.php"; ?>
    </div>
</a>
