<div class="card card__twitter">
    <div class="card__image" style=""></div>
    <div class="card__content">
        <div class="card__category">twitter</div>
        <p class="card__text">
        Lorem ipsum dolor sit amet, consectetur adipisicing
        <span class="card__tag">#tempor</span>
        </p>
        <?php require "partials/cards/_caption-03.php"; ?>
    </div>
</div>
