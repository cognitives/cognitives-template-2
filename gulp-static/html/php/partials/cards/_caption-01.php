<div class="card__caption">
    <div class="card__icon-social social_share_toggler ">
        <div class="tooltip">
            <div class="tooltip__link tooltip__link--facebook"></div>
            <div class="tooltip__link tooltip__link--twitter"></div>
            <div class="tooltip__link tooltip__link--google-plus"></div>
        </div>
    </div>
    <div class="card__avatar" style="background-image:url('static/images/data/avatar-08.jpg');">
        <div class="card__avatar-follow"></div>
    </div>
    <div class="card__author">By Username</div>
    <div class="card__date">8 OCT 2015</div>
    <div class="card__read-time">12 min read</div>
</div>
