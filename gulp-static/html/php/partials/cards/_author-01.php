<div class="author">
    <div class="author__image" style="background-image:url('static/images/data/background-17.jpg');"></div>
    <div class="author__avatar" style="background-image:url('static/images/data/avatar-08.jpg');"></div>

    <h1 class="author__name">Username</h1>
    <p class="author__title">Position & Title</p>

    <a href="#" class="button">FOLLOW +</a>

    <div class="author__links">
        <a href="#" class="author__link author__link--followers">
        <strong>487</strong>
        Followers
        </a>
        <a href="#" class="author__link author__link--following">
        <strong>24</strong>
        Following
        </a>
        <a href="#" class="author__link author__link--articles">
        <strong>46</strong>
        Articles
        </a>
    </div>
</div>
