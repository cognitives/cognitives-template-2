<div href="#" class="card card__instagram card__withoutImage">
    <div class="card__no-image"></div>
    <div class="card__content">
        <div class="card__category">instagram</div>
        <div class="card__tag">
            <span class="card__tag-paragraph">#inspiration</span>
            <span class="card__tag-paragraph">#everyday</span>
        </div>
        <?php require "partials/cards/_caption-03.php"; ?>
    </div>
</div>
