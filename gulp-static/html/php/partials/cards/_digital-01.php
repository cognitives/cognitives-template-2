<a href="#" class="card card--digital">
    <div class="card__image" style="background-image:url('static/images/data/background-07.jpg');"></div>
    <div class="card__content">
        <div class="card__category">digital</div>
        <h1 class="card__heading">Restrict yourself when dealing with clients.</h1>
    </div>
</a>
