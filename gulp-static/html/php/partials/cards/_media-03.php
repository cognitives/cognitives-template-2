<a href="#" class="card card--light card--media">
    <div class="card__image" style="background-image:url();"></div>
    <div class="card__content">
        <div class="card__category">media</div>
        <h1 class="card__heading">How much media do we really consume?</h1>
        <p class="card__text">This is Photoshop's version of lorem ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum...</p>
        <?php require "partials/cards/_caption-01.php"; ?>
    </div>
</a>
