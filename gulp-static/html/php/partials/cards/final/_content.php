<?php require 'partials/cards/_btn-overlay.php'; ?>
<div class="card__image" style="background-image:url('static/images/data/background-16.jpg');">
    <div class="card-icon"></div>
</div>
<div class="card__content">
    <div class="content-section">
        <span class="card__category">Category</span>
        <h1 class="card__heading">Headline title consectet adipiscing elit ipsum Headline title consectet adipiscing elit ipsum Headline title consectet adipiscing elit ipsum</h1>
        <p class="card__text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        <div class="card__tag">
            <span class="card__tag-paragraph">#inspiration</span>
            <span class="card__tag-paragraph">#everyday</span>
        </div>
        <?php require "partials/cards/_caption-01.php"; ?>
    </div>
</div>
