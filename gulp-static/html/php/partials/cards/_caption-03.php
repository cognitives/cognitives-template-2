<div class="card__caption">
    <div class="card__avatar" style="background-image:url('static/images/data/avatar-08.jpg');">
        <div class="card__avatar-follow"></div>
    </div>
    <div class="card__author">@social user</div>
    <div class="card__date">8 Oct 2015</div>
</div>
