<a href="#" class="card card--category">
    <div class="card__image" style="background-image:url();"></div>
    <div class="card__content">
        <div class="card__category">category</div>
        <h1 class="card__heading">Headline title consectet adipiscing elit  ipsum</h1>
        <p class="card__text">This is Photoshop's version of lorem ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum...</p>
        <?php require "partials/cards/_caption-02.php"; ?>
    </div>
</a>
