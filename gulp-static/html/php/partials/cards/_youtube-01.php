<a href="#" class="card card__youtube withImage_content">
    <div class="card__image card__image--full card__image" style="background-image: url();">
        <div class="card__icon-youtube"></div>
    </div>
    <div class="card__content">
        <div class="card__category">youtube</div>
        <h1 class="card__heading">Use Art to Turn the World Inside Out</h1>
        <?php require "partials/cards/_caption-03.php"; ?>
    </div>
</a>
