<div class="card card__banner card__instagram" style="background-image:url('static/images/data/background-11.jpg')">
    <div class="card__content">
        <div class="card__category">instagram</div>
        <div class="card__tag">
        <span class="card__tag-paragraph">#inspiration</span>
        <span class="card__tag-paragraph">#everyday</span>
        </div>

        <?php require "partials/cards/_caption-05.php"; ?>
    </div>
</div>
