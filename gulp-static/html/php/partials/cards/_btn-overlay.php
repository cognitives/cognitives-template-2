<div class="btn_overlay">
    <button type="button" class="btnhide social-tooltip DeleteSocialBtn" title="Delete"><i class="fa fa-trash"></i></button>
    <button type="button" class="btnhide social-tooltip EditSocialBtn" title="Edit"><i class="fa fa-edit"></i></button>
    <button type="button" class="btnhide social-tooltip selected PinArticleBtn" title="Pin"><i class="fa fa-thumb-tack"></i></button>
</div>