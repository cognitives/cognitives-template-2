<div class="card__caption">
    <div class="card__avatar" style="background-image:url('static/images/data/avatar-08.jpg');">
        <div class="card__avatar-follow"></div>
        <!--<div class="card__avatar-unfollow"></div>-->
    </div>
    <a href="#" class="card__author">By Username</a>
    <div class="card__date">8 OCT 2015</div>
    <div class="card__read-time">12 min read</div>
</div>
