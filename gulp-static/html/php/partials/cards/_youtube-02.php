<a href="#" class="card card--light card__youtube card__imageOnly">
    <div class="card__image card__image--full card__image" style="background-image: url();"></div>
    <div class="card__content">
        <div class="card__category">youtube</div>
        <div class="card__icon-youtube"></div>
        <h1 class="card__heading">Use art to turn the world inside out</h1>
        <p class="card__text"></p>
        <?php require "partials/cards/_caption-04.php"; ?>
    </div>
</a>
