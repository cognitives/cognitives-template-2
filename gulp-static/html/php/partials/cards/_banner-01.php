<div class="card card__banner card--category full_screen" style="background-image:url()">
    <div class="card__content">
        <a href="#" class="card__category">Category</a>

        <h1 class="card__heading">Full width feature. Lorem <br> ipsum dolor sit amet.</h1>

        <?php require "partials/cards/_caption-01.php"; ?>
    </div>
</div>
