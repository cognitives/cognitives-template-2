<a href="#" class="card card--media">
    <div class="card__content">
        <div class="card__category">media</div>
        <h1 class="card__heading">The Life and Death of a Venture Funded Media Startup</h1>
        <?php require "partials/cards/_caption-01.php"; ?>
    </div>
</a>
