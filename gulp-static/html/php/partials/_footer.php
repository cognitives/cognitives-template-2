<footer class="footer">
  <div class="footer__container">
    <div class="footer__info">
      <a href="/" class="footer__logo"><img src="static/images/themeLogo_Footer.svg" class="img-responsive" alt="Logo"></a>
      <div class="social">
          <a href="#" class="fa_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
          <a href="#" class="fa_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
          <a href="#" class="fa_instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
          <a href="#" class="fa_youtube"><i class="fa fa-youtube" aria-hidden="true"></i></a>
          <a href="#" class="fa_linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
          <a href="#" class="fa_gplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
          <a href="#" class="fa_pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
          <a href="#" class="fa_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
      </div>
      <div class="footer__copyright">Copyright 2016</div>
    </div>
    <div class="footer__navigation">
      <div class="footer__navigation--col-half">
        <ul class="footer__navigation--col-1">
          <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 1</a></li>
          <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 2</a></li>
          <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 3</a></li>
          <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 4</a></li>
        </ul>
        <ul class="footer__navigation--col-2">
          <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 5</a></li>
          <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 6</a></li>
          <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Category 7</a></li>
        </ul>
      </div>
      <div class="footer__navigation--col-half">
        <ul class="footer__navigation--col-3">
          <li class="footer__navigation-item"><a href="/" class="footer__navigation-link">Contact us</a></li>
          <li class="footer__navigation-item"><a class="footer__navigation-link" href="#">Advertising</a></li>
          <li class="footer__navigation-item"><a class="footer__navigation-link" href="#">About Us</a></li>
          <li class="footer__navigation-item"><a class="footer__navigation-link" href="terms.php">Terms & Conditions</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>


<?php require "partials/_responsive-navigation.php" ?>
<div class="menu-overlay"></div>


<!-- Begin Ajax based My Platform Modal Popup -->
<div class="modal fade" id="AjaxLoginModal" tabindex="-1" role="dialog" aria-labelledby="AjaxLoginModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="modal-close x-close" data-dismiss="modal"><span class="sr-only">Close</span></button>

			<div class="modal-body nopadd"></div>
		</div>
	</div>
</div>
<!-- //End Ajax based Profile Modal Popup -->

</body>
</html>
