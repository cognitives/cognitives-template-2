<div class="footer">
    <div class="container">
        <h1 class="footer__heading">You might like...</h1>
        <div class="row">
            <div class="col-third">
                <?php require("partials/cards/_category-01.php") ?>
            </div>
            <div class="col-third">
                <?php require("partials/cards/_youtube-03.php") ?>
            </div>
            <div class="col-third">
                <?php require("partials/cards/_twitter-02.php") ?>
            </div>
        </div>
    </div>
</div>