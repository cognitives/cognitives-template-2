<div class="responsive-navigation" role="navigation">
    <div class="close-menu" style="display: block;"><span class="icon_close"></span></div>
    <ul class="responsive-navigation__list">
        <li class="responsive-navigation__item"><a href="blog.php" class="responsive-navigation__link">Blog</a></li>
        <li class="responsive-navigation__item"><a href="article.php" class="responsive-navigation__link">Article</a>
            <ul class="sub-menu">
                <li><a href="#">Lorem Ipsum</a></li>
                <li><a href="#">Lorem Ipsum Dolor</a></li>
                <li><a href="#">Lorem Ipsum Sit</a></li>
                <li><a href="#">Lorem Ipsum Amet</a></li>
            </ul>
        </li>
        <li class="responsive-navigation__item"><a href="card-styles.php" class="responsive-navigation__link">Card Styles</a>
            <ul class="sub-menu">
                <li><a href="#">Lorem Ipsum</a></li>
                <li><a href="#">Lorem Ipsum Dolor</a></li>
                <li><a href="#">Lorem Ipsum Sit</a></li>
                <li><a href="#">Lorem Ipsum Amet</a></li>
            </ul>
        </li>
        <li class="responsive-navigation__item"><a href="#" class="responsive-navigation__link">Technology</a></li>
        <li class="responsive-navigation__item"><a href="#" class="responsive-navigation__link">Digital</a></li>
        <li class="responsive-navigation__item"><a href="#" class="responsive-navigation__link">Innovation</a></li>
    </ul>
    </div>
</div>
