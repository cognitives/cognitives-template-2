<?php require "functions/functions.php"; ?>

<!DOCTYPE html>
<!--[if lte IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->
<!--[if gt IE 8]>
<html class="ie" lang="en-US">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
  <meta charset="UTF-8" />
  <title>Future Marketers</title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="static/images/favicon.png">

  <!--[if lte IE 8]><script type="text/javascript" src="ie.js"></script><![endif]-->
  <!--
  Country News stylesheet
  <link rel="stylesheet" href="http://ww2.countrynews.com.au/resources/countrynews/resources/deploy/all.min.css">
  -->

  <link rel="stylesheet" href="static/css/index.css" type="text/css" media="all">
  <script type="text/javascript" src="static/js/index.js"></script>

</head>

<body class="body">
<header class="header">
    <div class="header__container">
        <div class="col-left">
            <a href="/" class="header__logo" id="logo"></a>
        </div>
        <div class="col-right">
            <a class="header__button" href="#">Post</a>
            <a class="header__notifications" href="#">
                <div class="header__notifications-count">2</div>
            </a>
            <a class="header__link" href="#">Login</a>
            <a class="header__link" href="#">Sign up</a>

            <a href="index.php" class="header__logo header__logo--center" id="logo">
                <img src="static/images/themeLogo.svg" alt="logo">
            </a>
        </div>
    </div>
    <?php require "partials/_profile-menu.php" ?>

    <div class="header__navigation">
        <div class="container">
            <div class="col-left">
                <a id="header-responsive" href="#" class="header__navigation-responsive"></a>
                <ul class="header__navigation-list">
                    <li class="header__navigation-item"><a href="blog.php" class="header__navigation-link">Blog</a></li>
                    <li class="header__navigation-item"><a href="article.php" class="header__navigation-link header__navigation-link--selected">Article</a>
                        <ul class="sub-menu">
                            <li><a href="#">Lorem Ipsum</a></li>
                            <li><a href="#">Lorem Ipsum Dolor</a></li>
                            <li><a href="#">Lorem Ipsum Sit</a></li>
                            <li><a href="#">Lorem Ipsum Amet</a></li>
                        </ul>
                    </li>
                    <li class="header__navigation-item"><a href="card-styles.php" class="header__navigation-link">Card Styles</a></li>
                    <li class="header__navigation-item"><a href="#" class="header__navigation-link">Technology</a></li>
                    <li class="header__navigation-item"><a href="#" class="header__navigation-link">Digital</a></li>
                    <li class="header__navigation-item"><a href="#" class="header__navigation-link">Innovation</a></li>
                </ul>
            </div>
            <div class="col-right">
                <input class="header__search" type="text" placeholder="Search ...">
            </div>
        </div>
    </div>
</header>
