<div class="slides">
    <div class="slide video">
        <video controls preload="none" poster="static/images/data/thumbnail-full-article-01.png">
            <source src="static/images/data/video.mp4" type="video/mp4">
			<source src="static/images/data/video.webm" type="video/webm">
			<source src="static/images/data/video.ogg" type="video/ogg">
        </video>
        <div class="slide__content">
            <div class="slide__caption"><p>Image caption</p></div>
        </div>
    </div>
    <div class="slide" style="background-image: url('static/images/data/background-18.jpg')">
        <div class="slide__content">
            <div class="slide__caption"><p>Image caption</p></div>
        </div>
    </div>
    <div class="slide" style="background-image: url('static/images/data/background-15.jpg')">
        <div class="slide__content">
            <div class="slide__caption"><p>Image caption</p></div>
        </div>
    </div>
</div>
