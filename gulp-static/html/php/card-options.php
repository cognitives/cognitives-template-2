<?php require("partials/_header.php") ?>

<div class="page_section">
    
    <!-- Category -->
    <a href="#" class="card blocked_element video_card card__banner card--category full_screen">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Category Ads -->
    <a href="#" class="card blocked_element card__banner card--category card_ads full_screen">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Twitter -->
    <a href="#" class="card blocked_element card__banner card__twitter card_social full_screen">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Facebook -->
    <a href="#" class="card blocked_element card__banner card__facebook card_social full_screen">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Instagram -->
    <a href="#" class="card blocked_element card__banner card__instagram card_social full_screen">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Youtube -->
    <a href="#" class="card blocked_element card__banner card__youtube card_social full_screen">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Vimeo -->
    <a href="#" class="card blocked_element card__banner card__vimeo card_social full_screen">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    
    
    <!-- Category -->
    <a href="#" class="card blocked_element card__banner card--category full_screen without__image">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Category Ads -->
    <a href="#" class="card blocked_element card__banner card--category card_ads full_screen without__image">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Twitter -->
    <a href="#" class="card blocked_element card__banner card__twitter card_social full_screen without__image">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Facebook -->
    <a href="#" class="card blocked_element card__banner card__facebook card_social full_screen without__image">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    <!-- Instagram -->
    <a href="#" class="card blocked_element card__banner card__instagram card_social full_screen without__image">
        <?php require("partials/cards/final/_content.php") ?>
    </a>
    
    
    
    <div class="container">
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Col Two Thirds With Image in Full Height and Content</h2></div>
        </div>
        
        <div class="row">
            <!-- Category -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element video_card card--technology card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element video_card card--technology card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Category Ads -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card--technology card_ads card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card--technology card_ads card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Twitter -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__twitter card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__twitter card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Facebook -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__facebook card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__facebook card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Instagram -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__instagram card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__instagram card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Youtube -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__youtube card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__youtube card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Vimeo -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__vimeo card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__vimeo card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Col Two Thirds With Image in Half Width and Content</h2></div>
        </div>
        
        <div class="row">
            <!-- Category -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element video_card card--technology withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element video_card card--technology withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Category Ads -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card--technology card_ads withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card--technology card_ads withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Twitter -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__twitter card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__twitter card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Facebook -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__facebook card_social withImage_content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__facebook card_social withImage_content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Instagram -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__instagram card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__instagram card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Youtube -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__youtube card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__youtube card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Vimeo -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__vimeo card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__vimeo card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
        
        
        
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Col Two Thirds Without Image Content</h2></div>
        </div>
        
        <div class="row">
            <!-- Category -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card--technology without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card--technology without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Category Ads -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card--technology card_ads without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card--technology card_ads without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Twitter -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__twitter card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third-short">
                <a href="#" class="card blocked_element video_card card--technology withImage__content caption_bottom">
                        <?php require("partials/cards/final/_content.php") ?>
                    </a>

                <a href="#" class="card blocked_element card--technology card_ads withImage__content caption_bottom">
                        <?php require("partials/cards/final/_content.php") ?>
                    </a>
            </div>

            <!-- Facebook -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__facebook card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__facebook card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Instagram -->
            <div class="col-two-thirds">
                <a href="#" class="card blocked_element card__instagram card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            <div class="col-third">
                <a href="#" class="card blocked_element card__instagram card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
    </div>
    
    
    <div class="container">
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Col Half With Image in Full Height and Content</h2></div>
        </div>
        
        <div class="row">
            <!-- Category -->
            <div class="col-half">
                <a href="#" class="card blocked_element card--technology card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Category Ads -->
            <div class="col-half">
                <a href="#" class="card blocked_element card--technology card_ads card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Twitter -->
            <div class="col-half">
                <a href="#" class="card blocked_element video_card card__twitter card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Facebook -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__facebook card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Instagram -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__instagram card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Youtube -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__youtube card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Vimeo -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__vimeo card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Col Half With Image in Half Width and Content</h2></div>
        </div>
        
        <div class="row">
            <!-- Category -->
            <div class="col-half">
                <a href="#" class="card blocked_element card--technology withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Category Ads -->
            <div class="col-half">
                <a href="#" class="card blocked_element card--technology card_ads withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Twitter -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__twitter card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Facebook -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__facebook card_social withImage_content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Instagram -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__instagram card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Youtube -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__youtube card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Vimeo -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__vimeo card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
        
        
        
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Col Half Without Image Content</h2></div>
        </div>
        
        <div class="row">
            <!-- Category -->
            <div class="col-half">
                <a href="#" class="card blocked_element card--technology without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Category Ads -->
            <div class="col-half">
                <a href="#" class="card blocked_element card--technology card_ads without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Twitter -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__twitter card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Facebook -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__facebook card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Instagram -->
            <div class="col-half">
                <a href="#" class="card blocked_element card__instagram card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
    </div>
    
    
    <div class="container">
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Col Quarter With Image in Full Height and Content</h2></div>
        </div>
        
        <div class="row">
            <!-- Category -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element video_card card--technology card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Category Ads -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card--technology card_ads card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Twitter -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__twitter card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Facebook -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__facebook card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Instagram -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__instagram card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Youtube -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__youtube card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Vimeo -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__vimeo card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Col Quarter With Image in Half Width and Content</h2></div>
        </div>
        
        <div class="row">
            <!-- Category -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card--technology withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Category Ads -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card--technology card_ads withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Twitter -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__twitter card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Facebook -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__facebook card_social withImage_content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Instagram -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__instagram card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Youtube -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__youtube card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Vimeo -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__vimeo card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
        
        
        
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Col Quarter Without Image Content</h2></div>
        </div>
        
        <div class="row">
            <!-- Category -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card--technology without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Category Ads -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card--technology card_ads without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <!-- Twitter -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__twitter card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Facebook -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__facebook card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            <!-- Instagram -->
            <div class="col-quarter">
                <a href="#" class="card blocked_element card__instagram card_social without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
    </div>
    
    
    <div class="container">
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Third Short With Image in Full Height and Content</h2></div>
        </div>
        
        <div class="row">
            <div class="col-third-short">
                <!-- Category -->
                <a href="#" class="card blocked_element card--technology card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Twitter -->
                <a href="#" class="card blocked_element card__twitter card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <div class="col-third-short">
                <!-- Category Ads -->
                <a href="#" class="card blocked_element card--technology card_ads card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Twitter -->
                <a href="#" class="card blocked_element card__twitter card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            
            <div class="col-third-short">
                <!-- Facebook -->
                <a href="#" class="card blocked_element card__facebook card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Instagram -->
                <a href="#" class="card blocked_element card__instagram card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            
            <div class="col-third-short">
                <!-- Youtube -->
                <a href="#" class="card blocked_element card__youtube card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Vimeo -->
                <a href="#" class="card blocked_element card__vimeo card_social card--light">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Third Short With Image in Half Width and Content</h2></div>
        </div>
        
        <div class="row">
            <div class="col-third-short">
                <!-- Category -->
                <a href="#" class="card blocked_element card--technology withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Twitter -->
                <a href="#" class="card blocked_element card__twitter card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <div class="col-third-short">
                <!-- Category Ads -->
                <a href="#" class="card blocked_element card--technology card_ads withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Twitter -->
                <a href="#" class="card blocked_element card__twitter card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            
            <div class="col-third-short">
                <!-- Facebook -->
                <a href="#" class="card blocked_element card__facebook card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Instagram -->
                <a href="#" class="card blocked_element card__instagram card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            
            <div class="col-third-short">
                <!-- Youtube -->
                <a href="#" class="card blocked_element card__youtube card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Vimeo -->
                <a href="#" class="card blocked_element card__vimeo card_social withImage__content caption_bottom">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
        
        
        <div class="row">
            <div class="col-full" style="margin-bottom:30px;"><h2>Card Third Short Without Image Content</h2></div>
        </div>
        
        <div class="row">
            <div class="col-third-short">
                <!-- Category -->
                <a href="#" class="card blocked_element card--technology card--light without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Twitter -->
                <a href="#" class="card blocked_element card__twitter card_social card--light without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
            
            <div class="col-third-short">
                <!-- Category Ads -->
                <a href="#" class="card blocked_element card--technology card_ads card--light without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
                
                <!-- Twitter -->
                <a href="#" class="card blocked_element card__twitter card_social card--light without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>

            
            <div class="col-third-short">
                <!-- Facebook -->
                <a href="#" class="card blocked_element card__facebook card_social card--light without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>

                <!-- Instagram -->
                <a href="#" class="card blocked_element card__instagram card_social card--light without__image">
                    <?php require("partials/cards/final/_content.php") ?>
                </a>
            </div>
        </div>
    </div>
    
</div>

<?php require("partials/_footer.php") ?>