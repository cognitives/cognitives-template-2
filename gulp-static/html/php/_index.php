<?php require("partials/_header.php") ?>

<div class="page_section">
    <?php require("partials/cards/_banner-01.php") ?>

    <div class="container">
        <div class="row">
            <div class="col-two-thirds">
                <?php require("partials/cards/_category-01.php") ?>      
            </div>
            <div class="col-third-short">
                <?php require("partials/cards/_category-01.php") ?>
                <?php require("partials/cards/_twitter-03.php") ?>
            </div>
        </div>

        <div class="row">
            <div class="col-two-thirds"><?php require("partials/cards/_youtube-01.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_facebook-02.php") ?></div>
        </div>

        <div class="row">
            <div class="col-two-thirds"><?php require("partials/cards/_facebook-01.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_facebook-02.php") ?></div>
        </div>

        <div class="row">
            <div class="col-two-thirds"><?php require("partials/cards/_twitter-03.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_facebook-02.php") ?></div>
        </div>







        <div class="row">
            <div class="col-full"><?php require("partials/cards/_banner-06.php") ?></div>
            <?php //require("partials/_slides.php") ?>
        </div>


        <div class="row"><div class="col-md-12"><h1 class="footer__heading">3 Column With Images</h1></div></div>
        <div class="row">
            <div class="col-third"><?php require("partials/cards/_category-01.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_media-01.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_twitter-02.php") ?></div>

            <div class="col-third"><?php require("partials/cards/_youtube-01.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_instagram-01.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_vimeo-01.php") ?></div>

            <div class="col-third"><?php require("partials/cards/_digital-01.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_featured-writers-01.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_follow-01.php") ?></div>
        </div>

        <div class="row"><div class="col-md-12"><h1 class="footer__heading">3 Column Without Images</h1></div></div>
        <div class="row">
            <div class="col-third"><?php require("partials/cards/_facebook-03.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_media-03.php") ?></div>
            <div class="col-third"><?php require("partials/cards/_instagram-03.php") ?></div>
        </div>
    </div>
</div>

<?php require("partials/_footer.php") ?>