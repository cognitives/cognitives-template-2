var add_marker, generate_google_map, select_marker;

generate_google_map = function(element_name, coord) {
  var map;
  map = new google.maps.Map(document.getElementById(element_name), {
    center: new google.maps.LatLng(coord[0], coord[1]),
    panControl: false,
    position: new google.maps.LatLng(coord[0], coord[1]),
    scrollwheel: false,
    zoom: 10,
    styles: $.google_map_styles
  });
  return map;
};

add_marker = function(google_map, map, link) {
  var coord, marker, pin_image;
  pin_image = $("html").hasClass("ie8") ? "map-pin.png" : "map-pin.svg";
  coord = link.data("coord").split(",");
  $(link).on("click", function(e) {
    var pos;
    e.preventDefault();
    $(window).scrollTo(map, 600, {
      margin: true
    });
    $($.markers).each(function() {
      return this.setIcon($.images_path + "/" + pin_image);
    });
    select_marker(marker);
    if ($(this).data("pos")) {
      pos = new google.maps.LatLng($(this).data("pos").split(",")[0], $(this).data("pos").split(",")[1]);
    } else {
      pos = marker.getPosition();
    }
    return google_map.panTo(pos);
  });
  return marker = new google.maps.Marker({
    position: new google.maps.LatLng(coord[0], coord[1]),
    map: google_map,
    icon: $.images_path + "/" + pin_image
  });
};

select_marker = function(marker) {
  return marker.setIcon({
    url: $.images_path + "/map-marker.png",
    scaledSize: new google.maps.Size(22, 40)
  });
};
