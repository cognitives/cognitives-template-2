(($) ->
  "use strict"

  $.fn.tabs = (method) ->
    methods =
      init: (options)->
        @.tabs.settings = $.extend(@.tabs.defaults, options)

        @.each ->
          settings = $.fn.tabs.settings

          $links    = $(@).find(".#{settings.navigation}")
          $contents = $(@).find(".#{settings.content}")

          # Configure the default view
          $contents.hide()
          $contents.first().show()

          $links.first().addClass(settings.selectClass)

          # Add the event listeners
          $links.on "click", (e) ->
            e.preventDefault()

            $links.removeClass(settings.selectClass)
            $(@).addClass(settings.selectClass)

            $(".#{settings.content}").hide()
            $("##{$(@).data("target")}").show()


    methods.init.apply(@, arguments)

  # Default settings
  $.fn.tabs.defaults =
    navigation:  "tabs__navigation-link"
    content:     "tabs__content"
    selectClass: "tabs__navigation-link--selected"

  $.fn.tabs.settings = {}

) jQuery
