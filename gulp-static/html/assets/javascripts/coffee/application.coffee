$.resizeSections = () ->
  $window   = $(window)
  $sections = $(".full_screen")
  $height   = $window.height()

  # Resize on initial load
  $sections.each ->
    $(@).css("min-height", "#{$height}px")

  # Resize every time the window resizes
  $window.on "resize", ->
    $height = $window.height()
    $sections.each ->
      $(@).css("min-height", "#{$height}px")


$(document).ready ->

  $.resizeSections()


  # Slides
  $(".slides").slick
    # prevArrow: '<a href="#" class="slick-prev">Prev</a>'
    # nextArrow: '<a href="#" class="slick-next">Next</a>'
    arrows: false
    autoplay: true
    autoplaySpeed: 3000
    dots: true
    draggable: false
    slidesToShow: 1
    speed: 400

  # Profile menu
  $("#profile").on "click", (e) ->
    e.preventDefault()

    $("#header__menu").simplemodal
      modal: true
      overlayClose: true
      opacity: 0.9
      fixed: false
      zIndex: 11010
      closeHTML: ""
      autoPosition: false
      onShow: (object) ->
        header = $(".header__container").outerHeight()
        $(object.container).css("top", "#{header}px")


  # Social
  $(".card__icon-social").hover (->
    $(@).find(".tooltip").addClass("tooltip--visible")
  ), ->
    $(@).find(".tooltip").removeClass("tooltip--visible")


  #social Icon Toggler
$('.card__social-share').on 'click', (e) ->
  e.preventDefault()
  elem = $(this)
  if elem.hasClass('selected')
    $(this).removeClass 'selected'
  else
    $('.card__social-share').removeClass 'selected'
    $(this).addClass 'selected'
  return

handleSearch = ->
  $('#custom-search-button').on 'click', (e) ->
    e.preventDefault()
    if !$('#custom-search-button').hasClass('open')
      $('#custom-search-form-container').fadeIn 600
    else
      $('#custom-search-form-container').fadeOut 600
    return
  $('#custom-search-form').append '<a class="custom-search-form-close" href="javascript:;" title="Close Search Box"><span>x</span></a>'
  $('#custom-search-form-container a.custom-search-form-close').on 'click', (event) ->
    event.preventDefault()
    $('#custom-search-form-container').fadeOut 600
    return
  return

handleFullScreen = ->
  x = $(window).height()
  $('.fullscreen').css 'height', x + 'px'
  return

$(document).ready ->
  handleFullScreen()
  handleSearch()
  return
$(window).resize ->
  handleFullScreen()
  return

$(document).ready ->

    #! Navigation
    $navigation = '.responsive-navigation'
    $nextItem = '<span class="next-item"><i class="fa fa-angle-right"></i></span>'
    $back = '<li class="back"><a href="javascript:;"><i class="fa fa-chevron-left"></i>Back</a></li>'


    $(".responsive-navigation .responsive-navigation__list li:has(ul)").prepend $nextItem
    $(".responsive-navigation .responsive-navigation__list .sub-menu").prepend $back

    $('#header-responsive').on 'click', ->
        $('body').addClass('no-scroll');
        $('.responsive-navigation').addClass 'navigation-active'
        return

    $('.responsive-navigation .next-item').on 'click', (e) ->
        e.preventDefault()
        $(this).nextAll('.sub-menu').addClass 'navigation-active'
        return

    $('.responsive-navigation .back').on 'click', (e) ->
        e.preventDefault()
        $(this).parent('.sub-menu').removeClass 'navigation-active'
        return

    $('.close-menu').on 'click', ->
        $('.responsive-navigation').removeClass 'navigation-active'
        $('body').removeClass('no-scroll');
        return

    $('.menu-overlay').on 'click', ->
        $('.responsive-navigation').removeClass 'navigation-active'
        $('body').removeClass('no-scroll');
        return

    $(".header__navigation-list li:has(ul)").addClass("menu-item-has-children")
    $(".header__navigation-list li:has(ul) > a").addClass("has-child")
    return


  #Stop Video with Modal Close
  $('#videoModal').on 'hide.bs.modal', (e) ->
    $if = $(e.delegateTarget).find('iframe')
    src = $if.attr('src')
    $if.attr 'src', '/empty.html'
    $if.attr 'src', src
    return
  # Fill modal with content from link href
  $('#AjaxLoginModal').on 'show.bs.modal', (e) ->
    link = $(e.relatedTarget)
    $(this).find('.modal-body').load link.attr('href')
    return
  $('#AjaxUserProfileModal').on 'show.bs.modal', (e) ->
    link = $(e.relatedTarget)
    $(this).find('.modal-body').load link.attr('href')
    return
  $('#AjaxMyPlatformModal').on 'show.bs.modal', (e) ->
    link = $(e.relatedTarget)
    $(this).find('.modal-body').load link.attr('href')
    return


  # Text overflow handling
  cardHolder = ''
  $(window).load ->
    clearTimeout cardHolder
    cardHolder = setTimeout((->
      $(".card p, .card h1").dotdotdot({watch: true})
      return
    ), 750)
    return
