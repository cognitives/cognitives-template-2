module.exports = [
  # jQuery
  "jquery-2.1.0.min"
  "jquery-migrate-1.2.1.min"
  "bootstrap.min"

  # "jquery.browser"

  # NAVIGATION                           
  "jquery.bigslide.min"                           

  # Utilities
  "jquery.simplemodal"
  
  # IMAGES
  "jquery.slick.min" 
  
  # TEXT MANIPULATION
  "jquery.dotdotdot"

  # APPLICATION
  "application"

  # owl carousel
  "owl.carousel"
]
